module ally

go 1.13

require (
	github.com/go-httpproxy/httpproxy v0.0.0-20180417134941-6977c68bf38e
	github.com/go-openapi/spec v0.19.7
)
